# csv形式でクラスは0から整数でナンバリングしたものを与える
#require "facter" # マルチプロセス使用時
#require "parallel" # マルチプロセス使用時

class Dataset
  
  attr_reader :size,:class_num,:dim
  # データ数, クラス数, 次元
  
  # ファイル名とクラス数
  def initialize(file,class_num)
    puts("loading dataset: #{file}")
    @class_num=class_num
    @data=[] # データ
    @label=[] # ラベル
    file=File::open(file,"r")
    file.each_line do |line|
      tmp=line.chomp!.split(",")
      @data.push(tmp[0...tmp.size-1].map{|val|val.to_f})
      arr=Array.new(class_num,0.0)
      arr[tmp.last.to_i]=1.0
      @label.push(arr)
    end
    file.close
    @size=@data.size
    @dim=@data[0].size
    # @core=Facter["processorcount"].value.to_i # コア数(マルチプロセス使用時)
    puts("#{@size} data load. (dim: #{@dim}, class: #{@class_num})")
  end
  
  # n個のバッチをランダムに獲得
  def get_batch(n)
    data=[]
    label=[]
    #Parallel::each(0...n,in_processes:4) do # マルチプロセス使用時(システム次第)
    n.times do
      index=rand(@size)
      data.push(@data[index])
      label.push(@label[index])
    end
    return data,label
  end
  
  # 全データ取得
  def get_all_data
    return @data,@label
  end
  
end
