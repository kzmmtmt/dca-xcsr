puts("【自動実験スクリプト for DCA】")
system("title 実験スクリプト for DCA")
print("実験数を入力: ")

n=gets.to_i

puts("\n#{n}回の実験を開始します…\n\n")
sleep(2)

n.times do |i|
  print("\rプログラムを立ち上げています: #{i+1}/#{n}")
  system("start python main.py #{i}")
  sleep(2)
end

sleep(1)


