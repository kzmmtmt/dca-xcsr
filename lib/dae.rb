require "pycall"
require "numpy"

# Deep Autoencoder
class DAE
  
  # layer: [input, hidden1, ..., hidden_center]
  # 学習時のドロップアウト率(1でドロップアウトなし)
  def initialize(layer,dropout_rate)
    tf=PyCall::import_module("tensorflow")
    @layer=[layer,layer[0...layer.size-1].reverse].flatten
    # @layerは入力層から出力層の直前まで入る

    @depth=@layer.size # 入出力層を含めた深さ
    @dropout_rate=dropout_rate
    
    z=[]
    w=[nil] # nilはダミー
    b=[nil] # nilはダミー
    d=[] # ドロップアウト層
    @dropout=tf.placeholder(tf.float32) # 評価時/エンコード/デコード時は1

    
    # 学習時: encode=1 decode=0
    # 評価時: encode=1 decode=0
    # エンコード時: encode=1 decode=0
    # デコード時: encode=0 decode=1
    @encode=tf.placeholder(tf.float32) # 1の時入力が有効
    @decode=tf.placeholder(tf.float32) # 1の時入力が有効
    
    # 入力層
    z.push(tf.placeholder(tf.float32,[nil,@layer[0]]))
    d.push(tf.nn.dropout(z[0],1.0))
    @x=z[0] # 入力
    
    # 隠れ層(エンコード)
    for i in 1..@depth/2 do
      w.push(tf.Variable.new(tf.truncated_normal([@layer[i-1],@layer[i]]),name:"w#{i}"))
      b.push(tf.Variable.new(tf.truncated_normal([@layer[i]])*0.1,name:"b#{i}"))
      z.push(tf.nn.sigmoid(tf.matmul(d[i-1],w[i])+b[i]))
      d.push(tf.nn.dropout(z[i],@dropout))
    end
    
    # 中央層
    @encoded=d[@depth/2] # エンコードされた値
    @center=tf.placeholder(tf.float32,[nil,@layer[@depth/2]]) # デコード用入力(デコード時以外は0を入れる)
      
    # 隠れ層(デコード)
    for i in @depth/2+1...@depth do
      w.push(tf.Variable.new(tf.truncated_normal([@layer[i-1],@layer[i]]),name:"w#{i}"))
      b.push(tf.Variable.new(tf.truncated_normal([@layer[i]])*0.1,name:"b#{i}"))
      if i==@depth/2+1 then
        z.push(tf.nn.sigmoid(tf.matmul(tf.add(@encode*d[i-1],@decode*@center),w[i])+b[i]))
      else
        z.push(tf.nn.sigmoid(tf.matmul(d[i-1],w[i])+b[i]))
      end
      d.push(tf.nn.dropout(z[i],@dropout))
    end
      
    # 出力
    w_ae=tf.Variable.new(tf.truncated_normal([@layer[@depth-2],@layer[@depth-1]]),name:"w#{@depth}")
    b_ae=tf.Variable.new(tf.truncated_normal([@layer[@depth-1]])*0.1,name:"b#{@depth}")
    @y=tf.nn.sigmoid(tf.matmul(z[@depth-2],w_ae)+b_ae)
      
    # 学習
    @loss=tf.nn.l2_loss(@y-@x)
    @train=tf.train.AdamOptimizer.new.minimize(@loss)
    
    # 評価
    @error=tf.reduce_sum(tf.abs(@y-@x))
    
    # 初期化
    @session=tf.InteractiveSession.new
    @session.run(tf.global_variables_initializer)
    @saver=tf.train.Saver.new([w[1..@depth],b[1..@depth]].flatten)
    
    puts("--DAE--\nlayer: #{@layer}\ndropout_rate: #{@dropout_rate}\ndepth: #{@depth}\n")
  end
  
  # バッチ学習
  def train(batch_x)
    center=get_empty_array(batch_x.size,@layer[@depth/2])
    @session.run(@train,feed_dict:{@x=>batch_x,@dropout=>@dropout_rate,@center=>center,@encode=>1.0,@decode=>0.0})
  end
  
  # 評価
  def evaluate(x)
    tmp=@session.run([@loss,@error],feed_dict:{@x=>x,@dropout=>1.0,@center=>get_empty_array(x.size,@layer[@depth/2]),@encode=>1.0,@decode=>0.0})
    return tmp.map{|val|val.to_f/x.size}
  end
  
  # エンコード
  def encode(x)
    return @session.run(@encoded,feed_dict:{@x=>x,@dropout=>1.0,@encode=>1.0,@decode=>0.0}).tolist.to_a
  end
  
  # デコード
  # 入力は圧縮された次元
  def decode(x)
    return @session.run(@y,feed_dict:{@x=>get_empty_array(x.size,@layer[0]),@center=>x,@dropout=>1.0,@encode=>0.0,@decode=>1.0}).tolist.to_a
  end
  
  # 出力
  def output(x)
    return @session.run(@y,feed_dict:{@x=>x,@dropout=>1.0,@center=>get_empty_array(x.size,@layer[@depth/2]),@encode=>1.0,@decode=>0.0}).tolist.to_a
  end
  
  # 現在のモデルを保存
  def save(file)
    @saver.save(@session,file)
  end
  
  # 学習済みのモデルを復元
  def restore(file)
    @saver.restore(@session,file)
    puts("load model: #{file}")
  end
  
  private
  # @center用0埋めリスト
  def get_empty_array(n,size)
    center=[]
    n.times do
      center.push(Array.new(size,0))
    end
    return center
  end
 
end