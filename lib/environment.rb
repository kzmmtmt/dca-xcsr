require "./dataset"

# 環境
class Environment
  
  REWARD_CORRECT=1000 # 正解時の報酬
  REWARD_INCORRECT=0 # 不正解時の報酬
  
  attr_reader :class_num

  def self.max_reward
    return [REWARD_CORRECT,REWARD_INCORRECT].max
  end
  
  def initialize(dataset)
    @dataset=dataset
    @class_num=@dataset.class_num
  end
  
  def get_situation
    x,@y=@dataset.get_batch(1)
    x.flatten!
    @y.flatten!
    return x
  end
  
  def get_reward(act)
    if act==@y.index(1) then
      return REWARD_CORRECT
    end
    return REWARD_INCORRECT
  end
  
  # マルチステップ問題だったら要実装
  def is_eop?
    return true
  end
  
end