class Result
  attr_accessor :itr
  def initialize(name)
    @name=name
    @itr=[]
    @seed=[]
    @values=[]
  end

  def add(seed,list)
    @seed.push(seed)
    @values.push(list)
  end

  def print_title
    return ",#{@name},#{","*(@seed.size)},,"
  end

  def print_header
    return %Q<,#{@seed.join(",")},mean,center,,>
  end

  def print_line(i)
    value=@values.map{|v|v[i]}
    tmp=value.dup
    value.sort!
    center=value[value.size/2]
    if value.size%2==0 then
      center=(center+value[(value.size/2)-1])/2.0
    end
    return %Q<#{@itr[i]},#{tmp.join(",")},#{value.sum.to_f/value.size},#{center},,>
  end
end


if ARGV.size==2 then
  dir=ARGV[0]
  name=ARGV[1]
else
  puts("【ログファイルコレクター】")
  print("ディレクトリ名: ")
  dir=gets.chomp
  puts(Dir::glob("../#{dir}/*.csv"))
  print("\nキーワード: ")
  name=gets.chomp
end


list=[]
Dir::foreach("../#{dir}/") do |item|
  if item.include?(name+"-") then
    list.push("#{File::expand_path("../#{dir}/")}/#{item}")
  end
end
list.sort!

puts("#{list.size} files detected.")
puts("->#{list}")
if list.size==0 then
  puts("nothing to do.")
  exit
end

data=[]


file=File::open(list[0],"r")
file.each_line do |line|
  if file.lineno==1 then
    buf=line.chomp.split(",")
    buf.delete_at(0)
    buf.each do |item|
      data.push(Result.new(item))
    end
  else
    v=line.chomp.split(",")[0]
    data.each do |d|
      d.itr.push(v)
    end
  end
end
file.close

list.each do |f|
  file=File::open(f,"r")
  tmp=Array.new(data.size){|i|[]}
  file.each_line do |line|
    if file.lineno==1 then
      next
    end
    buf=line.chomp.split(",")
    buf.delete_at(0)
    buf.map!{|item|item.to_f}
    for i in 0...data.size do
      tmp[i].push(buf[i])
    end
  end
  for i in 0...data.size do
    data[i].add(f.chomp.split("-").last.split(".").first,tmp[i])
  end
  file.close
end

File::open("#{File::expand_path("../#{dir}/")}/#{name}.csv","w") do |f|
  str=""
  for i in 0...data.size do
    str+=data[i].print_title
  end
  str+="\n"
  for i in 0...data.size do
    str+=data[i].print_header
  end
  str+="\n"

  data[0].itr.size.times do |i|
    for j in 0...data.size do
      str+=data[j].print_line(i)
    end
    str+="\n"
  end
  f.puts(str)
  puts("output to #{name}.csv")
end
