@echo off
echo GPU版に書き変えています…
echo ※うまくいかない場合は管理者権限で実行してください
echo お待ち下さい
title GPU用に書き換え中
pip uninstall -y tensorflow
pip uninstall -y tensorflow-tensorboard
pip install tensorflow-gpu
echo 完了
echo 何かのキーを押して終了します
pause>nul