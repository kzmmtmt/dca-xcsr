require "./environment"
require "./classifier_set"
require "./parameter"

# ruby ver 2.4以上で対応

# XCSR
class XCSR
  
  attr_reader :population,:itr
  
  def initialize(environment)
    @env=environment
    @population=ClassifierSet.new # [P]
    @id_count=0 # 次に振られる分類子のID
    @itr=0 # イタレーション
  end
  
  def run_experiment(train_num) # train_num: 学習イタレーション数
    action_set_1=ClassifierSet.new
    rho_1=0
    train_num.times do
      sigma=@env.get_situation
      match_set=generate_match_set(sigma)
      pa=generate_prediction_array(match_set)
      act=select_action(pa)
      action_set=generate_action_set(match_set,act)
      rho=@env.get_reward(act)
      unless action_set_1.is_empty? then
        p=rho_1+Parameter::XCSR::GAMMA*pa.select{|value|value!=nil}.max
        update_set(action_set_1,p)
        run_ga(action_set_1)
      end
      if @env.is_eop? then
        update_set(action_set,rho)
        run_ga(action_set)
        action_set_1.clear
      else
        action_set_1=action_set
        rho_1=rho
      end
      @itr+=1
      #print("\r#{@itr}: #{@population.size}")
    end
  end
  
  # 評価
  def evaluate(num)
    ok=0
    ng=0
    p_save=Marshal::load(Marshal::dump(@population))
    id_save=@id_count
    num.times do
      sigma=@env.get_situation
      match_set=generate_match_set(sigma)
      pa=generate_prediction_array(match_set)
      act=select_action(pa,true)
      rho=@env.get_reward(act)
      if rho==Environment::REWARD_CORRECT then
        ok+=1
      else
        ng+=1
      end
    end
    @population=p_save
    @id_count=id_save
    return ok.to_f/num
  end
  
  # 分類子追加
  def add_classifier(cl)
    cl.id=id_count
    cl.ts=@itr
    @population.add(cl)
  end
  
  protected
  
  def generate_match_set(sigma)
    match_set=ClassifierSet.new
    while match_set.is_empty? do
      @population.classifiers.each do |cl|
        if cl.does_match_input?(sigma) then
          match_set.add(cl)
        end
      end
      if match_set.number_of_different_actions<@env.class_num then # THETA_MNAと同様
        @population.add(generate_covering_classifier(match_set,sigma))
        delete_from_population
        match_set.clear
      end
    end
    return match_set
  end
  
  def generate_covering_classifier(match_set,sigma)
    cl=Classifier.new(id_count,@itr)
    cl.initialize_state(sigma)
    cl.a=match_set.random_action_not_present_in_match_set(@env.class_num)
    cl.p=Parameter::XCSR::P_I
    cl.epsilon=Parameter::XCSR::EPSILON_I
    cl.f=Parameter::XCSR::F_I
    cl.as=1
    cl.n=1
    return cl
  end
  
  def generate_prediction_array(match_set)
    pa=Array.new(@env.class_num,nil)
    fsa=Array.new(@env.class_num,0.0)
    match_set.classifiers.each do |cl|
      if pa[cl.a]==nil then
        pa[cl.a]=cl.p*cl.f
      else
        pa[cl.a]+=cl.p*cl.f
      end
      fsa[cl.a]+=cl.f
    end
    @env.class_num.times do |i|
      if fsa[i]!=0.0 then
        pa[i]=pa[i]/fsa[i]
      end
    end
    return pa
  end
  
  def select_action(pa,evaluate=false)
    not_nil_actions=[]
    pa.each_with_index do |value,i|
      if value!=nil then
        not_nil_actions.push(i)
      end
    end
    if rand<Parameter::XCSR::P_EXPLR&&!evaluate then
      return not_nil_actions.sample
    else
      actions=[]
      max=pa[not_nil_actions.max_by{|i|pa[i]}]
      not_nil_actions.each do |action|
        if pa[action]==max then
          actions.push(action)
        end
      end
      return actions.sample 
    end
  end
  
  def generate_action_set(match_set,act)
    return ClassifierSet.new(match_set.classifiers.select{|cl|cl.a==act})      
  end
  
  def update_set(action_set,p)
    sum_of_n=action_set.classifiers.map{|cl|cl.n}.sum
    action_set.classifiers.each do |cl|
      cl.exp+=1
      if cl.exp<1.0/Parameter::XCSR::BETA then
        cl.p+=(p-cl.p).to_f/cl.exp
        cl.epsilon+=((p-cl.p).abs-cl.epsilon).to_f/cl.exp
        cl.as+=(sum_of_n-cl.as).to_f/cl.exp
      else
        cl.p+=Parameter::XCSR::BETA*(p-cl.p).to_f
        cl.epsilon+=Parameter::XCSR::BETA*((p-cl.p).abs-cl.epsilon).to_f
        cl.as+=Parameter::XCSR::BETA*(sum_of_n-cl.as).to_f
      end
    end
    update_fitness(action_set)
    if Parameter::XCSR::DO_ACTION_SET_SUBSUMPTION then
      do_action_set_subsumption(action_set)
    end
  end
  
  def update_fitness(action_set)
    accuracy_sum=0
    action_set.classifiers.each do |cl|
      if cl.epsilon<Parameter::XCSR::EPSILON_0 then
        cl.kappa=1
      else
        cl.kappa=Parameter::XCSR::ALPHA*((cl.epsilon.to_f/Parameter::XCSR::EPSILON_0)**(-Parameter::XCSR::NU))
      end
      accuracy_sum+=cl.kappa*cl.n
    end
    action_set.classifiers.each do |cl|
      cl.f+=Parameter::XCSR::BETA*(cl.kappa*cl.n/accuracy_sum-cl.f)
    end
  end
  
  def run_ga(action_set)
    unless @itr-action_set.classifiers.inject(0.0){|sum,cl|sum+cl.ts*cl.n}.to_f/action_set.classifiers.map{|cl|cl.n}.sum>Parameter::XCSR::THETA_GA then
      return
    end
    action_set.classifiers.each do |cl|
      cl.ts=@itr
    end
    parent1=action_set.select_offspring
    parent2=action_set.select_offspring
    child1=parent1.copy(id_count,@itr)
    child2=parent2.copy(id_count,@itr)
    child1.f/=parent1.n.to_f # 追加
    child2.f/=parent2.n.to_f # 追加
    child1.n=child2.n=1
    child1.exp=child2.exp=0
    if rand<Parameter::XCSR::CHI then
      apply_crossover(child1,child2)
      child1.p=child2.p=(parent1.p+parent2.p)/2.0
      child1.epsilon=child2.epsilon=(parent1.epsilon+parent2.epsilon)/2.0
      child1.f=child2.f=(parent1.f+parent2.f)/2.0
    end
    [child1,child2].each do |child|
      child.f*=0.1
      child.apply_mutation(@env.class_num)
      if Parameter::XCSR::DO_GA_SUBSUMPTION then
        if parent1.does_subsume?(child) then
          parent1.n+=1
          # 条件緩和必要?
        elsif parent2.does_subsume?(child) then
          parent2.n+=1
          # 条件緩和必要?
        else
          insert_in_population(child)
        end
      else
        insert_in_population(child)
      end
      delete_from_population
    end
  end
  
  def apply_crossover(cl1,cl2)
    x,y=rand(cl1.length),rand(cl1.length)
    if x>y then
      x,y=y,x
    end
    for i in x..y do
      cl1.c[i],cl2.c[i]=cl2.c[i],cl1.c[i]
      cl1.s[i],cl2.s[i]=cl2.s[i],cl1.s[i]
    end
  end
  
  def insert_in_population(cl)
    @population.classifiers.each do |c|
      if c.is_equal_in_condition_and_action?(cl)
        c.n+=1
        return
      end
    end
    @population.add(cl)
  end
  
  def delete_from_population
    if @population.classifiers.map{|item|item.n}.sum<=Parameter::XCSR::N then
      return
    end
    @population.delete
  end
  
  def do_action_set_subsumption(action_set)
    cl=nil
    action_set.classifiers.each do |c|
      if c.could_subsume? then
        if cl==nil||c.is_more_general?(cl) then
          cl=c
        end        
      end
    end
    if cl!=nil then
      action_set.classifiers.each do |c|
        if cl.is_more_general?(c)
          cl.n+=c.n
          ### 条件緩和必要?
          action_set.classifiers.delete(c)
          @population.classifiers.delete(c)
        end
      end
    end      
  end
  
  # idを管理
  def id_count
    @id_count+=1
    return @id_count-1
  end
 
  
end