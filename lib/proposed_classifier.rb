require "./classifier"

# 提案手法用分類子
class ProposedClassifier<Classifier

  HEADER="id,c(encoded),s(encoded),c(decoded),s(decoded),a,p,epsilon,f,exp,ts,as,n,kappa,gt"
  
  def initialize_state(sigma,list)
    @c=sigma
    @s=list
  end
  
  # cは元の次元に, sはそのまま
  def apply_mutation(env)
    c=env.get_original_features(@c)
    flag=false
    for i in 0...c.size do
      if rand<Parameter::XCSR::MYU then
        c[i]+=rand*2.0*Parameter::XCSR::M-Parameter::XCSR::M
        flag=true
      end
    end
    if flag then
      c.map!{|val|if val>1 then 1.0 elsif val<0 then 0.0 else val end}
      @c=env.get_compressed_features(c)
    end
    flag=false
    for i in 0...@s.size do
      if rand<Parameter::XCSR::MYU then
        @s[i]+=rand*2.0*Parameter::XCSR::M-Parameter::XCSR::M
        flag=true
      end
    end
    if flag then
      @s.map!{|val|if val>1 then 1.0 elsif val<0 then 0.0 else val end}
    end
    if rand<Parameter::XCSR::MYU then
      actions=Array.new(env.class_num){|idx|idx}
      actions.delete(@a)
      @a=actions.sample
    end
  end
 
  
  def is_equal_in_condition_and_action?(cl)
    if cl.a!=@a then
      return false
    end
    for i in 0...@c.size do
      unless @c[i]-@s[i]-Parameter::XCSR::TOLERANCE<=cl.c[i]&&@c[i]+@s[i]+Parameter::XCSR::TOLERANCE>=cl.c[i] then ####
      #if @c[i]!=cl.c[i]||@s[i]!=cl.s[i] then
        return false
      end
    end
    return true
  end
  
    # cは元の次元に, sも
  def apply_mutation_real(env)
    c=env.get_original_features(@c)
    s=env.get_original_features(@s)
    flag=[false,false]
    for i in 0...c.size do
      if rand<Parameter::XCSR::MYU then
        c[i]+=rand*2.0*Parameter::XCSR::M-Parameter::XCSR::M
        flag[0]=true
      end
      if rand<Parameter::XCSR::MYU then
        s[i]+=rand*2.0*Parameter::XCSR::M-Parameter::XCSR::M
        flag[1]=true
      end
    end
    if flag[0] then
      c.map!{|val|if val>1 then 1.0 elsif val<0 then 0.0 else val end}
      @c=env.get_compressed_features(c)
    end
    if flag[1] then
      s.map!{|val|if val>1 then 1.0 elsif val<0 then 0.0 else val end}
      @s=env.get_compressed_features(s)
    end
    flag=false
    for i in 0...@s.size do
      if rand<Parameter::XCSR::MYU then
        @s[i]+=rand*2.0*Parameter::XCSR::M-Parameter::XCSR::M
        flag=true
      end
    end
    if rand<Parameter::XCSR::MYU then
      actions=Array.new(env.class_num){|idx|idx}
      actions.delete(@a)
      @a=actions.sample
    end
  end
  
      # cは元の次元に, sも(バイナリ用)
  def apply_mutation_real_bin(env)
    c=env.get_original_features(@c)
    s=env.get_original_features(@s)
    flag=[false,false]
    for i in 0...c.size do
      if rand<Parameter::XCSR::MYU then
        c[i]+=rand*2.0*Parameter::XCSR::M-Parameter::XCSR::M
        flag[0]=true
      end
      if rand<Parameter::XCSR::MYU then
        s[i]+=rand*2.0*Parameter::XCSR::M-Parameter::XCSR::M
        flag[1]=true
      end
    end
    if flag[0] then
      c.map!{|val|if val>1 then 1.0 elsif val<0 then 0.0 else val end}
      @c=env.get_compressed_features(c)
    end
    if flag[1] then
      s.map!{|val|if val>0.5 then 1.0 else Parameter::XCSR::TOLERANCE end}
      @s=env.get_compressed_features(s)
    end
    flag=false
    for i in 0...@s.size do
      if rand<Parameter::XCSR::MYU then
        @s[i]+=rand*2.0*Parameter::XCSR::M-Parameter::XCSR::M
        flag=true
      end
    end
    if rand<Parameter::XCSR::MYU then
      actions=Array.new(env.class_num){|idx|idx}
      actions.delete(@a)
      @a=actions.sample
    end
  end
  
  # decode情報を追加
  def to_s(env)
    c=env.get_original_features(@c).map{|val|if val>1 then 1.0 elsif val<0 then 0.0 else val.round(2) end}.join(" ")
    s=env.get_original_features(@s).map{|val|if val>1 then 1.0 elsif val<0 then 0.0 else val.round(2) end}.join(" ")
    return [@id,@c.map{|v|v.round(2)}.join(" "),@s.map{|v|v.round(2)}.join(" "),c,s,@a,@p,@epsilon,@f,@exp,@ts,@as,@n,@kappa,@gt].join(",")
  end
  
end