require "./xcsr"
require "./proposed_classifier"
require "./proposed_environment"

# 提案手法用XCSR(XCSRクラスの継承)
class ProposedXCSR<XCSR

  def get_population_to_s
    return @population.classifiers.map{|cl|cl.to_s(@env)}.join("\n")
  end
  
  protected
  
  def generate_covering_classifier(match_set,sigma)
    cl=ProposedClassifier.new(id_count,@itr)
    #cl.initialize_state(sigma)
    cl.initialize_state(sigma,@env.get_rand_bin)
    cl.a=match_set.random_action_not_present_in_match_set(@env.class_num)
    cl.p=Parameter::XCSR::P_I
    cl.epsilon=Parameter::XCSR::EPSILON_I
    cl.f=Parameter::XCSR::F_I
    cl.as=1
    cl.n=1
    return cl
  end
  
  def run_ga(action_set)
    unless @itr-action_set.classifiers.inject(0.0){|sum,cl|sum+cl.ts*cl.n}.to_f/action_set.classifiers.map{|cl|cl.n}.sum>Parameter::XCSR::THETA_GA then
      return
    end
    action_set.classifiers.each do |cl|
      cl.ts=@itr
    end
    parent1=action_set.select_offspring
    parent2=action_set.select_offspring
    child1=parent1.copy(id_count,@itr)
    child2=parent2.copy(id_count,@itr)
    child1.f/=parent1.n.to_f # 追加
    child2.f/=parent2.n.to_f # 追加
    child1.n=child2.n=1
    child1.exp=child2.exp=0
    if rand<Parameter::XCSR::CHI then
      apply_crossover(child1,child2)
      child1.p=child2.p=(parent1.p+parent2.p)/2.0
      child1.epsilon=child2.epsilon=(parent1.epsilon+parent2.epsilon)/2.0
      child1.f=child2.f=(parent1.f+parent2.f)/2.0
    end
    [child1,child2].each do |child|
      child.f*=0.1
      child.apply_mutation_real_bin(@env) # 変更
      if Parameter::XCSR::DO_GA_SUBSUMPTION then
        if parent1.does_subsume?(child) then
          parent1.n+=1
          # 条件緩和必要?
        elsif parent2.does_subsume?(child) then
          parent2.n+=1
          # 条件緩和必要?
        else
          insert_in_population(child)
        end
      else
        insert_in_population(child)
      end
      delete_from_population
    end
  end
  
  def apply_crossover(cl1,cl2)
    cl1_c,cl1_s=@env.get_original_features(cl1.c),@env.get_original_features(cl1.s)
    cl2_c,cl2_s=@env.get_original_features(cl2.c),@env.get_original_features(cl2.s)
    x,y=rand(cl1_c.length),rand(cl1_c.length)
    if x>y then
      x,y=y,x
    end
    for i in x..y do
      cl1_c[i],cl2_c[i]=cl2_c[i],cl1_c[i]
      cl1_s[i],cl2_s[i]=cl2_s[i],cl1_s[i]
    end
    cl1.set_state(@env.get_compressed_features(cl1_c),@env.get_compressed_features(cl1_s))
    cl2.set_state(@env.get_compressed_features(cl2_c),@env.get_compressed_features(cl2_s))
    cl1.c.map!{|val|if val>1 then 1.0 elsif val<0 then 0.0 else val end}
    cl1.s.map!{|val|if val>1 then 1.0 elsif val<0 then 0.0 else val end}
    cl2.c.map!{|val|if val>1 then 1.0 elsif val<0 then 0.0 else val end}
    cl2.s.map!{|val|if val>1 then 1.0 elsif val<0 then 0.0 else val end}
  end
  
end