require "./environment"

module Parameter
  
  DIGIT=3 # 有効桁数
    
  module NN
    DROPOUT_RATE=0.8
    LAYER=[196,49,13,4]
  end
  
  module XCSR
    TOLERANCE=0.0 # 許容値 ## 独自
    N=1000
    P_EXPLR=1 # action選択時のランダム率
    P_I=0.01
    EPSILON_I=0.01
    F_I=0.01
    GAMMA=0.71
    BETA=0.2
    EPSILON_0=Environment::max_reward*0.01
    ALPHA=0.1
    NU=5
    THETA_GA=12
    CHI=0.8
    THETA_DEL=20
    DELTA=0.1
    THETA_SUB=20
    S0=1.0
    M=0.1
    MYU=0.04
    DO_ACTION_SET_SUBSUMPTION=true
    DO_GA_SUBSUMPTION=true
  end
  
end
