Path=File.expand_path(File.dirname(__FILE__))
require Path+"/bmp.rb"

def to_img(list,name,n=1)
  dim=Math.sqrt(list.size).to_i
  array=list.each_slice(dim).to_a
  img=BitMap.new(dim*n,dim*n)
  for y in 0...dim do
    for x in 0...dim do
      if array[y][x]<0 then
        for i in 0...n do
          for j in 0...n do
            img.pset(n*x+i,n*y+j,255,0,0)
          end
        end
      else
        value=255-(array[y][x]*255).to_i
        for i in 0...n do
          for j in 0...n do
            img.pset(n*x+i,n*y+j,value,value,value)
          end
        end
      end
    end
  end
  img.write(name)
end

data=ARGV[0]

puts("loading #{data}...")
lines=[]
labels=[]
File::open(data,"r") do |f|
  f.each_line do |line|
    buf=line.chomp.split(",")
    lines.push(buf[0...buf.size-1].map{|item|item.to_f})
    labels.push(buf.last.to_i)
  end
end
dim=Math::sqrt(lines[0].size).to_i
puts("  -- #{lines.size} data (#{dim} dimensions)")
img=[]


lines.each_with_index do |line,index|
  print("\rpeocessing #{index+1}/#{lines.size}")
  to_img(line,"img/#{index}.png",10)
end
