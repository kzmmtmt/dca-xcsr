Adress=4

length=Adress+2**Adress

file=File::open("data.csv","w")

for a in 0...2**Adress do
  for r in 0...2**(2**Adress) do
    tmp=(sprintf("%0#{2**Adress}d",r.to_s(2).to_i).reverse!.to_i(2))&(2**a) > 0 ? "1" : "0"
    file.puts (sprintf("%0#{Adress}d",a.to_s(2).to_i)+sprintf("%0#{2**Adress}d",r.to_s(2).to_i)+tmp).chars.to_a.join(",")
  end
end

file.close
