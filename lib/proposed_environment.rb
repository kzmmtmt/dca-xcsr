require "./environment"

# 環境(提案手法用)
class ProposedEnvironment<Environment

  attr_reader :class_num,:dim
  
  def initialize(dataset,dca)
    super(dataset)
    @dca=dca
    @dim=@dataset.dim # 元次元
  end
  
  def get_situation
    x,@y=@dataset.get_batch(1)
    x=@dca.encode(x).flatten
    @y.flatten!
    return x
  end
  
  def get_rand_bin
    x=Array.new(@dataset.dim){|i|rand(2).to_f}
    return @dca.encode([x]).flatten
  end
  
  # エンコード
  def get_compressed_features(x)
    return @dca.encode([x]).flatten
  end
  
  # デコード
  def get_original_features(x)
    return @dca.decode([x]).flatten
  end
  
end