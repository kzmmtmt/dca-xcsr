require "./dataset"
require "./dca"
require "./proposed_xcsr"
require "./parameter"
require "pp"

class ProposedSystem
  
  attr_reader :train_set,:test_set,:dca,:proposed_xcsr,:xcsr,:env,:env_xcsr
  
  def initialize(train_set,test_set,hidden_layer,seed)
    puts("=====[ Proposed System ]=====")
    @train_set=train_set
    @test_set=train_set==test_set ? nil:test_set
    @dca=DCA.new([@train_set.dim,hidden_layer].flatten,@train_set.class_num,Parameter::NN::DROPOUT_RATE,seed:seed)
    @env=ProposedEnvironment.new(@train_set,@dca)
    @proposed_xcsr=ProposedXCSR.new(@env)
    @dca_itr=0
  end
  
  def read_model(file)
    @dca.restore(file)
  end
  
  # ログの保存ファイル, モデルの保存ファイル
  def train_dca(n,interval,batch_size:100,log:nil,save:nil,save_in_interval:false)
    puts("start training dca (epoch=#{n}, interval=#{interval})")
    buf=["loss_ae","loss_class","error","accuracy"]
    str="epoch,#{@test_set!=nil ? (buf.map{|v|"[train]"+v}+buf.map{|v|"[test]"+v}).join(","):buf.join(",")}\n"
    n.times do |i|
      x,y=@train_set.get_batch(batch_size)
      @dca.train(x,y)
      if i%interval==0 then
        str+=dca_evaluate
        if save_in_interval&&save!=nil&&i>0 then
          @dca.save("#{save}-#{@dca_itr}")
        end
      end
      @dca_itr+=1
    end
    str+=dca_evaluate
    puts("training finished.")
    if log!=nil then
      File::open(log+".csv","w") do |f|
        f.puts(str)
      end
      puts("saved dca log: #{log}.csv")
    end
    if save!=nil then
      @dca.save(save)
      puts("saved model: #{save}.*")
    end
  end
  
  def dca_evaluate
    x,y=@train_set.get_all_data
    tmp=@dca.evaluate(x,y)
    puts("#{@test_set!=nil ? "[train] ":""}#{@dca_itr}: loss_ae=#{tmp[0].round(Parameter::DIGIT)} loss_class=#{tmp[1].round(Parameter::DIGIT)} error=#{tmp[2].round(Parameter::DIGIT)} accuracy=#{tmp[3].round(Parameter::DIGIT)}")
    if @test_set!=nil then
      x,y=@test_set.get_all_data
      tmp2=@dca.evaluate(x,y)
      puts("[test] #{@dca_itr}: loss_ae=#{tmp2[0].round(Parameter::DIGIT)} loss_class=#{tmp2[1].round(Parameter::DIGIT)} error=#{tmp2[2].round(Parameter::DIGIT)} accuracy=#{tmp2[3].round(Parameter::DIGIT)}")
    end
    return "#{@dca_itr},#{tmp.join(",")}#{@test_set!=nil ? ","+tmp2.join(","):""}\n"
  end
  
  def train_proposed_xcsr(n,interval,test:1000,log:nil,save:nil)
    puts("start training proposed xcsr (iteration=#{n}, interval=#{interval}, N=#{Parameter::XCSR::N})")
    str="iteration,performance,population size\n"
    p=@proposed_xcsr.evaluate(test)*100
    puts("#{@proposed_xcsr.itr} : #{p.round(Parameter::DIGIT)}%, #{@proposed_xcsr.population.size}")
    str+="#{@proposed_xcsr.itr},#{p.round(Parameter::DIGIT)},#{@proposed_xcsr.population.size}\n"
    n.times do
      @proposed_xcsr.run_experiment(interval)
      p=@proposed_xcsr.evaluate(test)*100
      puts("#{@proposed_xcsr.itr} : #{p.round(Parameter::DIGIT)}%, #{@proposed_xcsr.population.size}")
      str+="#{@proposed_xcsr.itr},#{p.round(Parameter::DIGIT)},#{@proposed_xcsr.population.size}\n"
    end
    if log!=nil then
      File::open(log+".csv","w") do |f|
        f.puts(str)
      end
      puts("saved proposed xcsr log: #{log}.csv")
    end
    if save!=nil then
      File::open(save+".csv","w") do |f|
        f.puts(ProposedClassifier::HEADER)
        f.puts(@proposed_xcsr.get_population_to_s)
      end 
      puts("saved proposed xcsr classifiers: #{save}.csv")     
    end
  end
  
  # xcsrを用意する
  def set_xcsr(file=nil,empty:false)
    @env_xcsr=Environment.new(@train_set)
    @xcsr=XCSR.new(@env_xcsr)
    
    if empty then
      return
    end
    
    if file==nil then
      classifiers=@proposed_xcsr.population.classifiers
    else # ファイルから
      classifiers=[]
      puts("loading classifiers from file: #{file}.csv")
      File::open(file+".csv","r") do |f|
        f.each_line do |line|
          if f.lineno==1 then
            next
          end
          buf=line.split(",")
          cl=ProposedClassifier.new(nil,nil)
          #cl.set_state(buf[1].split(" ").map{|val| val.to_f},buf[2].split(" ").map{|val| val.to_f})
          cl.set_state(buf[3].split(" ").map{|val| val.to_f},buf[4].split(" ").map{|val| val.to_f})
          cl.a=buf[5].to_i
          cl.p,cl.epsilon,cl.f=buf[6..8].map{|val| val.to_f}
          cl.exp=buf[9].to_i
          cl.as,cl.n=buf[11..12].map{|val| val.to_i}
          cl.kappa=buf[13].to_f
          classifiers.push(cl)
        end
      end
    end
    
    # どこまでコピーするか
    classifiers.each do |cl|
      c=Classifier.new(nil,nil)
      #c.set_state(@env.get_original_features(cl.c),@env.get_original_features(cl.s))
      c.set_state(cl.c,cl.s)
      c.p=cl.p # 0
      #c.p=0.01
      #c.epsilon=cl.epsilon # 要考慮 # 0
      c.epsilon=Parameter::XCSR::EPSILON_I
      #c.f=cl.f # 要考慮 # 0
      c.f=Parameter::XCSR::F_I
      #c.f=0
      #c.exp=cl.exp # 要考慮 # 0
      c.exp=0
      #c.as=cl.as # 1
      c.as=1
      #c.n=cl.n # 要考慮 # 1
      c.n=1
      #c.kappa=cl.kappa
      c.a=cl.a
      @xcsr.add_classifier(c)
    end
    
    puts("#{classifiers.size} classifiers imported")
  end
  
  def train_xcsr(n,interval,test:1000,log:nil,save:nil)
    puts("start training xcsr (iteration=#{n}, interval=#{interval}, N=#{Parameter::XCSR::N})")
    str="iteration,performance,population size\n"
    p=@xcsr.evaluate(test)*100
    puts("#{@xcsr.itr} : #{p}%, #{@xcsr.population.size}")
    str+="#{@xcsr.itr},#{p},#{@xcsr.population.size}\n"
    n.times do
      @xcsr.run_experiment(interval)
      p=@xcsr.evaluate(test)*100
      puts("#{@xcsr.itr} : #{p}%, #{@xcsr.population.size}")
      str+="#{@xcsr.itr},#{p},#{@xcsr.population.size}\n"
    end
    if log!=nil then
      File::open(log+".csv","w") do |f|
        f.puts(str)
      end
      puts("saved xcsr log: #{log}.csv")
    end
    if save!=nil then
      File::open(save+".csv","w") do |f|
        f.puts(Classifier::HEADER)
        f.puts(@xcsr.population.to_s)
      end 
      puts("saved xcsr classifiers: #{save}.csv")     
    end
  end
  
end
