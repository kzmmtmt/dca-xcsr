require "./classifier"

# ruby ver 2.4以上で対応

# 分類子のセット
class ClassifierSet
  
  attr_reader :classifiers
  
  def initialize(list=nil)
    if list==nil then
      @classifiers=[]
    else
      @classifiers=list
    end
  end
  
  def size
    return @classifiers.size
  end
  
  # 分類子セットが空かどうか
  def is_empty?
    return @classifiers.empty?
  end
  
  # 分類子を追加
  def add(cl)
    @classifiers.push(cl)
  end
  
  # 中身を空に
  def clear
    @classifiers.clear
  end
  
  # 含まれているactionの数を計算
  def number_of_different_actions
    list=[]
    @classifiers.each do |cl|
      if cl.a!=nil&&!list.include?(cl.a) then # リストにないactionならば
        list.push(cl.a)
      end
    end 
    return list.size
  end
  
  # 含まれていないactionのうちランダムで1つ返す
  def random_action_not_present_in_match_set(classes)
    tmp=Array.new(classes,true)
    @classifiers.each do |cl|
      tmp[cl.a]=false
    end
    buf=[]
    tmp.each_with_index do |value,i|
      if value then
        buf.push(i)
      end
    end
    return buf.sample
  end

  
  # 分類子を1つ消す
  def delete
    av_fitness_in_population=@classifiers.map{|item|item.f}.sum/@classifiers.map{|item|item.n.to_f}.sum
    vote=@classifiers.map{|item|item.deletion_vote(av_fitness_in_population)}
    vote_sum=vote.sum
    choice_point=rand*vote_sum
    vote_sum=0
    @classifiers.each_with_index do |cl,i|
      vote_sum+=vote[i]
      if vote_sum>choice_point then
        if cl.n>1 then
          cl.n-=1
        else
          @classifiers.delete(cl)
        end
        return
      end
    end
  end
  
  def select_offspring
    choice_point=rand*@classifiers.map{|cl|cl.f}.sum
    fitness_sum=0
    @classifiers.each do |cl|
      fitness_sum+=cl.f
      if fitness_sum>=choice_point then
        return cl
      end
    end
  end
  
  def to_s
    return @classifiers.join("\n")
  end
  
end