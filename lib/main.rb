require "./dataset"
require "./proposed_system"
require "./parameter"
require "irb"
require "pp"
require "optparse"

seed=0
TRAIN_SET="mnist/mnist_bin_max_38.db"
TEST_SET="mnist/mnist_bin_max_38_test.db" # TRAIN_SETと同じ場合はnilを入れる
CLASS=2

# 実行するコード
def run(sys)
  #sys.read_model(PATH+"mnist/196-49-13-4")
  #sys.read_model(path("exp/196-49-13-4"))
  sys.train_dca(2000,1000,log:path("exp/nn"),save:path("exp/a"))
  #sys.train_proposed_xcsr(500,100,log:path("exp/log_encoded"),save:path("exp/model_encoded"))
  #sys.set_xcsr(path("1111/model_encoded-0"))
  #sys.set_xcsr(empty:true)
  #sys.train_xcsr(200,100,log:path("1111/log_proposed"),save:path("1111/model_proposed"))
  #sys.train_xcsr(200,100)
end


PATH=File::expand_path("..",File::dirname(__FILE__))+"/" # 現在のパス

# helpとタイプされたときに表示
def help
  puts("if you want to learn proposed system, you type...")
  puts("#sys.read_model(model_name)")
  puts("#sys.train_dca(n,interval,batch_size:100,log:nil,save:nil,save_in_interval:false)")
  puts("#sys.train_proposed_xcsr(n,interval,test:1000,log:nil,save:nil)")
  puts("#sys.set_xcsr(file=nil,empty:false)")
  puts("#sys.train_xcsr(n,interval,test:1000,log:nil,save:nil)")
  puts("and, this program is on the 'PATH'=#{PATH}.")
  puts("if you want to show the methods which the instance has, type '#instance.show_methods' or '#instance.show_public_methods'.")
  puts("\n")
end

def path(file_name)
  p=File::dirname(PATH+file_name)
  unless File::directory?(p)
    Dir::mkdir(p)
    puts("made directory #{p}")
  end
  return PATH+file_name+$output_suffix
end

interactive=false
$output_suffix=""

option=OptionParser.new
option.on("-i","--interactive","start program with interactive mode") do
  interactive=true
  puts("start [interactive mode]")
end

option.on("-s","--output-suffix SUFFIX","suffix of output file name") do |value|
  $output_suffix="-"+value
  seed=value.to_i
end
option.parse(ARGV)

if seed!=nil then
  srand(seed)
end

puts("starting up (@ #{Time.now})")
if seed!=nil then
  puts("seed=#{seed}")
end
train_set=Dataset.new(PATH+"dataset/"+TRAIN_SET,CLASS)
if TEST_SET!=nil then
  test_set=Dataset.new(PATH+"dataset/"+TEST_SET,CLASS)
else
  test_set=train_set
end
sys=ProposedSystem.new(train_set,test_set,Parameter::NN::LAYER,seed)

if interactive then
  puts("interactive mode started!")
  puts("type 'help' if you need help.")
  ARGV=[]
  binding.irb
  puts("\nprogram finished.")
else
  run(sys)
  puts("finished.(@ #{Time.now})")
end
