require 'rmagick'

if ARGV.size!=2 then
  puts("引数は2つです(w,h)")
  exit
end

w=ARGV[0].to_i
h=ARGV[1].to_i
filename="catenated.png"

imgs=[]

dir=Dir::open(".")
dir.each do |f|
  if f.match?(".png") then
    imgs.push(f)
  end
end

imgs.sort_by!{|item|File::basename(item,".png").to_i}

img_arry=[]
for i in 0...h do
  image=Magick::ImageList.new
  imgs[w*i...w*(i+1)].each do |item|
  image+=Magick::ImageList.new(item)
  end
  img_arry.push(image.append(false))
end

img=Magick::ImageList.new
img_arry.each do |item|
  img.push(item)
end
img.append(true).write(filename)
