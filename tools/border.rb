require 'rmagick'

DIRNAME="border"
POINT=16

dir=Dir::open(".")
draw=Magick::Draw.new
draw.pointsize=30
dir.each do |f|
  if f.match?(".png") then
    image=Magick::ImageList.new(f)
    image=image.scale(0.3)
    draw.annotate(image,0,0,5,22,File::basename(f,".png"))
    image=image.border(2,2,"black")
    image.write("#{DIRNAME}/#{f}")
  end
end

