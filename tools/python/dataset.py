# -*- coding: utf-8 -*-

import csv
import numpy as np
import random

# テスト用データセット
class Dataset:

  # ファイル名とクラス数
  def __init__(self,input_file,class_num):
    print("loading dataset: %s"%input_file)
    self.class_num=class_num
    self.data=[]
    self.label=[]
    with open(input_file,"r") as f:
      r=csv.reader(f)
      tmp=[]
      for row in r:
        tmp=[float(i) for i in row]
        d=tmp[0:len(tmp)-1]
        l=[0.0]*self.class_num
        l[int(tmp[len(tmp)-1])]=1.0
        self.data.append(d)
        self.label.append(l)
    self.size=len(self.data)
    self.dim=len(self.data[0])
    print("%d data load. (dim: %d, class: %d)"%(self.size,self.dim,self.class_num))

  # n個のバッチをランダムに獲得
  def get_batch(self,n):
    data=[]
    label=[]
    for i in range(0,n):
      index=random.randint(0,self.size-1)
      data.append(self.data[index])
      label.append(self.label[index])
    return data,label
    
  # 全データ取得
  def get_all_data(self):
    return self.data,self.label

