MERGIN=0.1
lines=[]
File::open(ARGV[0],"r") do |f|
  f.each_line do |line|
    tmp=[]
    buf=line.chomp.split(",")
    c=buf[0].split(" ").map{|item|item.to_f}
    s=buf[1].split(" ").map{|item|item.to_f}
    lower=[]
    upper=[]
    for i in 0...c.size do
      lower=c[i]-s[i]-MERGIN
      upper=c[i]+s[i]+MERGIN
      if lower<=0 && upper>=1 then
        tmp.push(-1)
      elsif upper>=1 then
        tmp.push(1)
      elsif lower<=0 then
        tmp.push(0)
      else
        tmp.push(1-upper<lower ? 1 :0)
      end
    end
    lines.push(tmp)
  end
end

File::open("cl.csv","w") do |f|
  lines.each do |item|
    f.puts("#{item.join(",")},0")
  end
end



