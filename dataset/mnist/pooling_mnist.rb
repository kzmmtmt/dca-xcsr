data=ARGV[0]
size=ARGV[1].to_i # プーリングサイズ

puts("loading #{data}...")
lines=[]
labels=[]
File::open(data,"r") do |f|
  f.each_line do |line|
    buf=line.chomp.split(",")
    lines.push(buf[0...buf.size-1].map{|item|item.to_f})
    labels.push(buf.last.to_i)
  end
end
dim=Math::sqrt(lines[0].size).to_i
puts("  -- #{lines.size} data (#{dim} dimensions)")
img=[]
puts("converting to 2 dimension...")
lines.each do |line|
  img.push(line.each_slice(dim).to_a)
end

puts("pooling to 1/#{size} size")
pooled_imgs=[]
img.each do |image|
  pooled_img=[]
  for y in 0...dim/size do
    for x in 0...dim/size do
      tmp=[]
      for i in 0...size do
        for j in 0...size do
          tmp.push(image[y*size+i][x*size+j])
        end
      end
      pooled_img.push(tmp.max>0.5 ? 1 : 0) # 最大プーリング(bin)
    end
  end
  pooled_imgs.push(pooled_img)
end
puts("writing pooling_minst.csv...")
File::open("pooling_mnist.csv","w") do |f|
  for i in 0...labels.size do
    f.puts("#{pooled_imgs[i].join(",")},#{labels[i]}")
  end
end
puts("finished!")
