# -*- coding: utf-8 -*-

from dca import DCA
from dataset import Dataset
import os
import sys
import tensorflow as tf
import numpy as np
import random as r

suffix=""
seed=None
if len(sys.argv)==2:
  seed=int(sys.argv[1])
  suffix="-"+str(seed)
  print("suffix=%s"%seed)
if seed!=None:
  r.seed(seed)
  np.random.seed(seed)
  tf.set_random_seed(seed)

train_set=Dataset("../../dataset/mnist/mnist_bin_max_38.db",2)
test_set=Dataset("../../dataset/mnist/mnist_bin_max_38_test.db",2)

path=os.path.dirname(os.path.abspath(__file__))
f=open("exp/nn_log%s.csv"%suffix,"w")

if test_set!=None:
  str="epoch,[train]loss_ae,[train]loss_class,[train]error,[train]accuracy,[test]loss_ae,[test]loss_class,[test]error,[test]accuracy\n"
else:
  str="epoch,loss_ae,loss_class,error,accuracy\n"

dca=DCA([train_set.dim,196,49,13,4],train_set.class_num,0.8)
#dca.restore("a")
#nn.restore(File::expand_path(File::dirname(__FILE__))+"/a")
for i in range(0,150001):
  x,y=train_set.get_batch(100)
  dca.train(x,y)
  if i%1000==0:
    x,y=train_set.get_all_data()
    tmp=dca.evaluate(x,y)
    if test_set!=None:
      x,y=test_set.get_all_data()
      tmp2=dca.evaluate(x,y)
      print("[train] %d: loss_ae=%f loss_class=%f error=%f accuracy=%f"%(i,tmp[0],tmp[1],tmp[2],tmp[3]))
      print("[test] %d: loss_ae=%f loss_class=%f error=%f accuracy=%f"%(i,tmp2[0],tmp2[1],tmp2[2],tmp2[3]))
      str+="%d,%f,%f,%f,%f,%f,%f,%f,%f\n"%(i,tmp[0],tmp[1],tmp[2],tmp[3],tmp2[0],tmp2[1],tmp2[2],tmp2[3])
    else:
      print("%d: loss_ae=%f loss_class=%f error=%f accuracy=%f"%(i,tmp[0],tmp[1],tmp[2],tmp[3]))
      str+="%d,%f,%f,%f,%f\n"%(i,tmp[0],tmp[1],tmp[2],tmp[3])
dca.save(path+"/exp/196-49-13-4"+suffix)
f.write(str)
f.close()
