require "./parameter"

# 分類子
class Classifier
  
  HEADER="id,c,s,a,p,epsilon,f,exp,ts,as,n,kappa,gt"
  
  attr_accessor :a,:p,:epsilon,:f,:as,:n,:exp,:kappa,:id,:ts,:gt
  attr_reader :c,:s
  
  def initialize(id,ts) 
    @id=id # 分類子固有のID
    @p=nil
    @epsilon=nil
    @f=nil
    @exp=0
    @ts=ts
    @as=nil
    @n=nil
    @kappa=0
    @a=nil
    @gt=ts # generate time
  end
  
  def set_state(c,s)
    @c=c
    @s=s
  end
  
  # sをcと同じ長さで初期化
  def initialize_state(sigma)
    @c=sigma
    @s=[]
    sigma.size.times do
      @s.push(rand*Parameter::XCSR::S0)
    end
  end
  
  # 入力と照合するか
  def does_match_input?(input)
    input.each_with_index do |value,i|
      unless @c[i]-@s[i]-Parameter::XCSR::TOLERANCE<=value&&@c[i]+@s[i]+Parameter::XCSR::TOLERANCE>=value then
        return false
      end
    end
    return true
  end
  
  def deletion_vote(av_fitness_in_population)
    vote=@as*@n
    if @exp>Parameter::XCSR::THETA_DEL&&@f/@n<Parameter::XCSR::DELTA*av_fitness_in_population then
      vote*=av_fitness_in_population/(@f/@n)
    end
    return vote
  end
  
  def could_subsume?
    if @exp>Parameter::XCSR::THETA_SUB&&@epsilon<Parameter::XCSR::EPSILON_0 then
      return true
    end
    return false
  end
  
  def is_more_general?(cl)
    @c.size.times do |i|
      unless @c[i]-@s[i]-Parameter::XCSR::TOLERANCE<=cl.c[i]-cl.s[i]&&cl.c[i]+cl.s[i]<=@c[i]+@s[i]+Parameter::XCSR::TOLERANCE then
        return false
      end
    end
    #if @s.sum!=cl.s.sum then ### 本当に合ってる?
    #  return true
    #end
    return true
  end
  
  def does_subsume?(cl)
    if @a==cl.a&&could_subsume?&&is_more_general?(cl) then
      return true
    end
    return false
  end
  
  def length
    return @c.size
  end
  
  # ディープコピー
  def copy(id,ts)
    tmp=Marshal::load(Marshal::dump(self))
    tmp.id=id
    tmp.gt=ts
    return tmp
  end
  
  def apply_mutation(action_num)
    flag=[false,false]
    for i in 0...@c.size do
      if rand<Parameter::XCSR::MYU then
        @c[i]+=rand*2.0*Parameter::XCSR::M-Parameter::XCSR::M
        flag[0]=true
      end
      if rand<Parameter::XCSR::MYU then
        @s[i]+=rand*2.0*Parameter::XCSR::M-Parameter::XCSR::M
        flag[1]=true
      end
    end
    @c.map!{|val|if val>1 then 1.0 elsif val<0 then 0.0 else val end} if flag[0]
    @s.map!{|val|if val>1 then 1.0 elsif val<0 then 0.0 else val end} if flag[1]
    if rand<Parameter::XCSR::MYU then
      actions=Array.new(action_num){|idx|idx}
      actions.delete(@a)
      @a=actions.sample
    end
  end
  
  def is_equal_in_condition_and_action?(cl)
    if cl.a!=@a then
      return false
    end
    for i in 0...@c.size do
      #unless @c[i]-@s[i]-Parameter::XCSR::TOLERANCE<=cl.c[i]&&@c[i]+@s[i]+Parameter::XCSR::TOLERANCE>=cl.c[i] then ####
      if @c[i]!=cl.c[i]||@s[i]!=cl.s[i] then
        return false
      end
    end
    return true
  end
  
  def to_s
    return [@id,@c.map{|v|v.round(2)}.join(" "),@s.map{|v|v.round(2)}.join(" "),@a,@p,@epsilon,@f,@exp,@ts,@as,@n,@kappa,@gt].join(",")
  end
end