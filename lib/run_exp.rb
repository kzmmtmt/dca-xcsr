puts("【自動実験スクリプト】")
system("title 実験スクリプト")
print("実験数を入力: ")

n=gets.to_i

print("並列で実行しますか? Y/N: ")
p=gets
if p.chomp.downcase=="y" then
  parallel=true
else
  parallel=false
end

puts("\n#{n}回の実験を#{parallel ?"並列で":""}開始します…\n\n")
sleep(2)

n.times do |i|
  print("\rプログラムを立ち上げています: #{i+1}/#{n} ")
  system("title 実験スクリプト(#{i+1}/#{n})")
  if parallel then
    system("start ruby main.rb -s #{i}")
  else
    system("ruby main.rb -s #{i}")
  end
  sleep(2)
end

sleep(1)


