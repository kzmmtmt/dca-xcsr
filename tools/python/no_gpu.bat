@echo off
echo GPU不要版に書き変えています…
echo ※うまくいかない場合は管理者権限で実行してください
echo お待ち下さい
title GPU不要版に書き換え中
pip uninstall -y tensorflow-gpu
pip uninstall -y tensorflow-tensorboard
pip install tensorflow
echo 完了
echo 何かのキーを押して終了します
pause>nul