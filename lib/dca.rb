require "pycall"
require "numpy"

# Deep Classification Autoencoder
class DCA
  
  # layer: [input, hidden1, ..., hidden_center]
  # class_num: クラス数
  # 学習時のドロップアウト率(1でドロップアウトなし)
  def initialize(layer,class_num,dropout_rate,seed:nil)
    os=PyCall::import_module("os")
    os.environ["TF_CPP_MIN_LOG_LEVEL"]="2"
    tf=PyCall::import_module("tensorflow")
    
    if seed!=nil then
      tf.set_random_seed(seed)
      Numpy.random.seed(seed)
      PyCall::import_module("random").seed(seed)
    end

    @layer=[layer,layer[1...layer.size-1].reverse].flatten
    # @layerは入力層から出力層の直前まで入る
    tf.reset_default_graph

    @depth=@layer.size+1 # 入出力層を含めた深さ
    @dropout_rate=dropout_rate
    
    z=[]
    w=[nil] # nilはダミー
    b=[nil] # nilはダミー
    d=[] # ドロップアウト層
    @dropout=tf.placeholder(tf.float32) # 評価時/エンコード/デコード時は1

    # 学習時: encode=1 decode=0
    # 評価時: encode=1 decode=0
    # エンコード時: encode=1 decode=0
    # デコード時: encode=0 decode=1
    @encode=tf.placeholder(tf.float32) # 1の時入力が有効
    @decode=tf.placeholder(tf.float32) # 1の時入力が有効
    
    # 入力層
    z.push(tf.placeholder(tf.float32,[nil,@layer[0]]))
    d.push(tf.nn.dropout(z[0],1.0))
    @x=z[0] # 入力
    
    # 隠れ層(エンコード)
    for i in 1..@depth/2 do
      w.push(tf.Variable.new(tf.truncated_normal([@layer[i-1],@layer[i]]),name:"w#{i}"))
      b.push(tf.Variable.new(tf.truncated_normal([@layer[i]])*0.1,name:"b#{i}"))
      z.push(tf.nn.sigmoid(tf.matmul(d[i-1],w[i])+b[i]))
      d.push(tf.nn.dropout(z[i],@dropout))
    end
    
    # 中央層
    @encoded=d[@depth/2] # エンコードされた値
    @center=tf.placeholder(tf.float32,[nil,@layer[@depth/2]]) # デコード用入力(デコード時以外は0を入れる)
      
    # 隠れ層(デコード)
    for i in @depth/2+1...@depth-1 do
      w.push(tf.Variable.new(tf.truncated_normal([@layer[i-1],@layer[i]]),name:"w#{i}"))
      b.push(tf.Variable.new(tf.truncated_normal([@layer[i]])*0.1,name:"b#{i}"))
      if i==@depth/2+1 then
        z.push(tf.nn.sigmoid(tf.matmul(tf.add(@encode*d[i-1],@decode*@center),w[i])+b[i]))
      else
        z.push(tf.nn.sigmoid(tf.matmul(d[i-1],w[i])+b[i]))
      end
      d.push(tf.nn.dropout(z[i],@dropout))
    end
    
    # 出力直前層
    w.push(tf.Variable.new(tf.truncated_normal([@layer[@depth-3],@layer[@depth-2]]),name:"w#{@depth-1}"))
    b.push(tf.Variable.new(tf.truncated_normal([@layer[@depth-2]])*0.1,name:"b#{@depth-1}"))
    z.push(tf.matmul(d[@depth-3],w[@depth-2])+b[@depth-2])
      
    # クラス出力
    w_class=tf.Variable.new(tf.truncated_normal([@layer[@depth-2],class_num]),name:"w_class")
    b_class=tf.Variable.new(tf.truncated_normal([class_num])*0.1,name:"b_class")
    @y_class=tf.nn.softmax(tf.matmul(z[@depth-2],w_class)+b_class)
      
    # AE出力
    w_ae=tf.Variable.new(tf.truncated_normal([@layer[@depth-2],@layer[0]]),name:"w_ae")
    b_ae=tf.Variable.new(tf.truncated_normal([@layer[0]])*0.1,name:"b_ae")
    @y_ae=tf.nn.sigmoid(tf.matmul(z[@depth-2],w_ae)+b_ae)
      
    # 学習
    @t=tf.placeholder(tf.float32,[nil,class_num])
    @loss_class=-1*tf.reduce_sum(@t*@y_class)
    @loss_ae=tf.nn.l2_loss(@y_ae-@x)
    @train_class=tf.train.AdamOptimizer.new.minimize(@loss_class)
    @train_ae=tf.train.AdamOptimizer.new.minimize(@loss_ae)
    
    # 評価
    correct_prediction=tf.equal(tf.argmax(@y_class,1),tf.argmax(@t,1))
    @accuracy=tf.reduce_mean(tf.cast(correct_prediction,tf.float32))
    @error=tf.reduce_sum(tf.abs(@y_ae-@x))
    
    # 初期化
    @session=tf.InteractiveSession.new
    @session.run(tf.global_variables_initializer)
    @saver=tf.train.Saver.new([w[1..@depth-1],b[1..@depth-1],w_class,b_class,w_ae,b_ae].flatten)
    
    puts("--DCA--\nlayer: #{@layer} + (#{class_num} [class] + #{@layer[0]} [ae])\ndropout_rate: #{@dropout_rate}\ndepth: #{@depth}\n")
  end
  
  # バッチ学習
  def train(batch_x,batch_t)
    center=get_empty_array(batch_t.size,@layer[@depth/2])
    @session.run(@train_class,feed_dict:{@x=>batch_x,@t=>batch_t,@dropout=>@dropout_rate,@center=>center,@encode=>1.0,@decode=>0.0})
    @session.run(@train_ae,feed_dict:{@x=>batch_x,@dropout=>@dropout_rate,@center=>center,@encode=>1.0,@decode=>0.0})
  end
  
  # 評価
  def evaluate(x,t)
    tmp=@session.run([@loss_ae,@loss_class,@error,@accuracy],feed_dict:{@x=>x,@t=>t,@dropout=>1.0,@center=>get_empty_array(t.size,@layer[@depth/2]),@encode=>1.0,@decode=>0.0})
    return [tmp[0..2].map{|val|val/x.size.to_f},tmp[3]].flatten
  end
  
  # エンコード
  def encode(x)
    return @session.run(@encoded,feed_dict:{@x=>x,@dropout=>1.0,@encode=>1.0,@decode=>0.0}).tolist.to_a
  end
  
  # デコード
  # 入力は圧縮された次元
  def decode(x)
    return @session.run(@y_ae,feed_dict:{@x=>get_empty_array(x.size,@layer[0]),@center=>x,@dropout=>1.0,@encode=>0.0,@decode=>1.0}).tolist.to_a
  end
  
  # 出力
  def output(x)
    tmp=@session.run([@y_class,@y_ae],feed_dict:{@x=>x,@dropout=>1.0,@center=>get_empty_array(x.size,@layer[@depth/2]),@encode=>1.0,@decode=>0.0})
    return tmp.map{|arry|arry.tolist.to_a}
  end
  
  # 現在のモデルを保存
  def save(file)
    @saver.save(@session,file)
  end
  
  # 学習済みのモデルを復元
  def restore(file)
    @saver.restore(@session,file)
    puts("load model: #{file}")
  end
  
  private
  # @center用0埋めリスト
  def get_empty_array(n,size)
    center=[]
    n.times do
      center.push(Array.new(size,0.0))
    end
    return center
  end
 
end
