# -*- coding: utf-8 -*-

import numpy as np
import tensorflow as tf
import os

# Deep Classification Autoencoder
class DCA:

  # layer: [input, hidden1, ..., hidden_center]
  # class_num: クラス数
  # 学習時のドロップアウト率(1でドロップアウトなし)
  def __init__(self,layer,class_num,dropout_rate):
    os.environ["TF_CPP_MIN_LOG_LEVEL"]="2"
    tmp=layer[1:len(layer)-1]
    tmp.reverse()
    self.layer=self.flatten([layer,tmp])
    # @layerは入力層から出力層の直前まで入る
    self.depth=len(self.layer)+1
    self.dropout_rate=dropout_rate
    tf.reset_default_graph()

    z=[]
    w=[None] # 0はダミー
    b=[None] # 0はダミー
    d=[] # ドロップアウト層
    self.dropout=tf.placeholder(tf.float32)

    # 学習時: encode=1 decode=0
    # 評価時: encode=1 decode=0
    # エンコード時: encode=1 decode=0
    # デコード時: encode=0 decode=1
    self.encode=tf.placeholder(tf.float32) # 1の時入力が有効
    self.decode=tf.placeholder(tf.float32) # 1の時入力が有効

    # 入力層
    z.append(tf.placeholder(tf.float32,[None,self.layer[0]]))
    d.append(tf.nn.dropout(z[0],1.0))
    self.x=z[0]

    # 隠れ層(エンコード)
    for i in range(1,int(self.depth/2)+1):
      w.append(tf.Variable(tf.truncated_normal([self.layer[i-1],self.layer[i]]),name="w%d"%i))
      b.append(tf.Variable(tf.truncated_normal([self.layer[i]])*0.1,name="b%d"%i))
      z.append(tf.nn.sigmoid(tf.matmul(d[i-1],w[i])+b[i]))
      d.append(tf.nn.dropout(z[i],self.dropout))

    # 中央層
    self.encoded=d[int(self.depth/2)] # エンコードされた値
    self.center=tf.placeholder(tf.float32,[None,self.layer[int(self.depth/2)]]) # デコード用入力(デコード時以外は0を入れる)

    # 隠れ層(デコード)
    for i in range(int(self.depth/2+1),self.depth-1):
      w.append(tf.Variable(tf.truncated_normal([self.layer[i-1],self.layer[i]]),name="w%d"%i))
      b.append(tf.Variable(tf.truncated_normal([self.layer[i]])*0.1,name="b%d"%i))
      if i==int(self.depth/2)+1:
        z.append(tf.nn.sigmoid(tf.matmul(tf.add(self.encode*d[i-1],self.decode*self.center),w[i])+b[i]))
      else:
        z.append(tf.nn.sigmoid(tf.matmul(d[i-1],w[i])+b[i]))
      d.append(tf.nn.dropout(z[i],self.dropout))

    # 出力直前層
    w.append(tf.Variable(tf.truncated_normal([self.layer[self.depth-3],self.layer[self.depth-2]]),name="w%d"%(self.depth-1)))
    b.append(tf.Variable(tf.truncated_normal([self.layer[self.depth-2]])*0.1,name="b%d"%(self.depth-1)))
    z.append(tf.matmul(d[self.depth-3],w[self.depth-2])+b[self.depth-2])

    # クラス出力
    w_class=tf.Variable(tf.truncated_normal([self.layer[self.depth-2],class_num]),name="w_class")
    b_class=tf.Variable(tf.truncated_normal([class_num])*0.1,name="b_class")
    self.y_class=tf.nn.softmax(tf.matmul(z[self.depth-2],w_class)+b_class)

    # AE出力
    w_ae=tf.Variable(tf.truncated_normal([self.layer[self.depth-2],self.layer[0]]),name="w_ae")
    b_ae=tf.Variable(tf.truncated_normal([self.layer[0]])*0.1,name="b_ae")
    self.y_ae=tf.nn.sigmoid(tf.matmul(z[self.depth-2],w_ae)+b_ae)

    # 学習
    self.t=tf.placeholder(tf.float32,[None,class_num])
    self.loss_class=-tf.reduce_sum(self.t*self.y_class)
    self.loss_ae=tf.nn.l2_loss(self.y_ae-self.x)
    self.train_class=tf.train.AdamOptimizer().minimize(self.loss_class)
    self.train_ae=tf.train.AdamOptimizer().minimize(self.loss_ae)

    # 評価
    correct_prediction=tf.equal(tf.argmax(self.y_class,1),tf.argmax(self.t,1))
    self.accuracy=tf.reduce_mean(tf.cast(correct_prediction,tf.float32))
    self.error=tf.reduce_sum(tf.abs(self.y_ae-self.x))

    # 初期化
    self.session=tf.InteractiveSession()
    self.session.run(tf.global_variables_initializer())
    tmp=self.flatten([w[1:self.depth],b[1:self.depth],w_class,b_class,w_ae,b_ae])
    self.saver=tf.train.Saver(tmp)
    
    print("--DCA--\nlayer: %s + (%d [class] + %d [ae])\ndropout_rate: %f\ndepth: %d\n"%(self.layer,class_num,self.layer[0],self.dropout_rate,self.depth))

  # バッチ学習
  def train(self,batch_x,batch_t):
    center=self.get_empty_array(len(batch_t),self.layer[int(self.depth/2)])
    self.session.run(self.train_class,feed_dict={self.x:batch_x,self.t:batch_t,self.dropout:self.dropout_rate,self.center:center,self.encode:1.0,self.decode:0.0})
    self.session.run(self.train_ae,feed_dict={self.x:batch_x,self.dropout:self.dropout_rate,self.center:center,self.encode:1.0,self.decode:0.0})

  # 評価
  def evaluate(self,x,t):
    tmp=self.session.run([self.loss_ae,self.loss_class,self.error,self.accuracy],feed_dict={self.x:x,self.t:t,self.dropout:1.0,self.center:self.get_empty_array(len(t),self.layer[int(self.depth/2)]),self.encode:1.0,self.decode:0.0})
    buf=tmp[0:3]
    return self.flatten([list(map(lambda n:float(n)/len(x),buf)),tmp[3]])

  # エンコード
  def encode(self,x):
    return self.session.run(self.encoded,feed_dict={self.x:x,self.dropout:1.0,self.encode:1.0,self.decode:0.0}).tolist()
  
  # デコード
  # 入力は圧縮された次元
  def decode(self,x):
    return self.session.run(selfr.y_ae,feed_dict={self.x:self.get_empty_array(len(x),self.layer[0]),self.center:x,self.dropout:1.0,self.encode:0.0,self.decode:1.0}).tolist()
  
  # 出力
  def output(self,x):
    tmp=self.session.run([self.y_class,self.y_ae],feed_dict={self.x:x,self.dropout:1.0,self.center:self.get_empty_array(len(x),self.layer[self.depth/2]),self.encode:1.0,self.decode:0.0})
    return tmp.tolist()
  
  # 現在のモデルを保存
  def save(self,f):
    self.saver.save(self.session,f)
  
  # 学習済みのモデルを復元
  def restore(self,f):
    self.saver.restore(self.session,f)
    print("load model: %s"%f)
  
  # @center用0埋めリスト
  def get_empty_array(self,n,size):
    return [[0.0]*size]*n

  # flattenにする
  def flatten(self,l):
    # フラットなリストとフリンジを用意
    flat_list=[]
    fringe=[l]
    while len(fringe)>0:
      node=fringe.pop(0)
      # ノードがリストであれば子要素をフリンジに追加
      # リストでなければそのままフラットリストに追加
      if isinstance(node,list):
        fringe=node+fringe
      else:
        flat_list.append(node)
    return flat_list

